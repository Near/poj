#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

struct Node {
  int rank, x, y;
  Node *parent;
  Node *children, *left;
  Node() {
    rank = x = y = 0;
    parent = this;
    children = left = NULL;
  }
  Node *top() {
    Node *res = this;
    while(res->parent != res) res = res->parent;
    return res;
  }
  void update(int dx, int dy) {
    x += dx, y += dy;
    Node *c = children;
    while(c != NULL) {
      c->update(dx, dy);
      c = c->left;
    }
  }
};

struct query {
  int f1, f2, t, index;
} q[10001];

bool operator<(query q1, query q2) {
  return q1.t < q2.t;
}

Node farms[40001];
int edge[40001][3], n, m, k, f1, f2, d, ans[10001];
char dir[40001];

void process(int e[3], char dir) {
  int i = e[0], j = e[1], dist = e[2], diffx = 0, diffy = 0;
  if(dir == 'E' || dir == 'W') diffx = dir == 'E' ? dist : -dist;
  else diffy = dir == 'N' ? dist : -dist;
  diffx = farms[i].x + diffx - farms[j].x, diffy = farms[i].y + diffy - farms[j].y;
  Node *p = farms[i].top(), *q = farms[j].top();
  if(p->rank < q->rank) { swap(p, q); diffx = -diffx, diffy = -diffy; }
  if(p->rank == q->rank) p->rank ++;
  q->parent = p;
  q->left = p->children;
  p->children = q;
  q->update(diffx, diffy);
}

int print(int i, int j) {
  if(farms[i].top() != farms[j].top()) return -1;
  else return abs(farms[i].x - farms[j].x) + abs(farms[i].y - farms[j].y);
}

int main() {
  cin >> n >> m;
  for(int i = 0; i < m; i ++) {
    for(int j = 0; j < 3; j ++)
      cin >> edge[i][j];
    cin >> dir[i];
  }
  cin >> k;
  for(int i = 0; i < k; i ++) {
    cin >> q[i].f1 >> q[i].f2 >> q[i].t;
    q[i].index = i;
  }
  sort(q, q+k);
  d = 0;
  for(int i = 0; i < k; i ++) {
    if(q[i].t > d) {
      for(int j = d+1; j <= q[i].t; j ++)
        process(edge[j-1], dir[j-1]);
      d = q[i].t;
    }
    ans[q[i].index] = print(q[i].f1, q[i].f2);
  }
  for(int i = 0; i < k; i ++) cout << ans[i] << endl;
  return 0;
}

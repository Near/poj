#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

const int MAXN = 40000, MAXM = 10000, MAXK = 10000;
int n, m, k, fa[MAXN+1], r[MAXN+1], ancestor[MAXN+1], dis[MAXN+1], ans[MAXK+1];
bool visited[MAXN+1];
typedef pair<int, int> PII;
vector<PII> edge[MAXN], query[MAXN];

void makeSet(int i) {
  fa[i] = i;
  r[i] = 0;
}

int findSet(int i) {
  if(fa[i] != i) fa[i] = findSet(fa[i]);
  return fa[i];
}

void unionSet(int i, int j) {
  i = findSet(i), j = findSet(j);
  if(r[i] < r[j]) swap(i, j);
  fa[j] = i;
  if(r[i] == r[j]) r[i] ++;
}

void LCA(int cur, int pre) {
  makeSet(cur);
  ancestor[cur] = cur;
  for(vector<PII>::size_type i = 0; i < edge[cur].size(); i ++) {
    int v = edge[cur][i].first, len = edge[cur][i].second;
    if(v != pre) {
      dis[v] = dis[cur] + len;
      LCA(v, cur);
      unionSet(cur, v);
      ancestor[findSet(cur)] = cur;
    }
  }
  visited[cur] = true;
  for(vector<PII>::size_type i = 0; i < query[cur].size(); i ++) {
    int v = query[cur][i].first, index = query[cur][i].second;
    if(visited[v]) {
      ans[index] = dis[cur] + dis[v] - 2 * dis[ancestor[findSet(v)]];
    }
  }
}

int main() {
  cin >> n >> m;
  int f1, f2, len;
  char dir;
  for(int i = 0; i < m; i ++) {
    cin >> f1 >> f2 >> len >> dir;
    edge[f1].push_back(PII(f2, len));
    edge[f2].push_back(PII(f1, len));
  }
  cin >> k;
  for(int i = 0; i < k; i ++) {
    cin >> f1 >> f2;
    ans[i] = -1;
    query[f1].push_back(PII(f2, i));
    query[f2].push_back(PII(f1, i));
  }
  dis[1] = 0;
  for(int i = 1; i <= n; i ++) {
    if(!visited[i]) LCA(i, i);
  }
  for(int i = 0; i < k; i ++) cout << ans[i] << endl;
  return 0;
}
